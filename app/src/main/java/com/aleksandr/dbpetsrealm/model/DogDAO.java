package com.aleksandr.dbpetsrealm.model;

import io.realm.RealmResults;

public interface DogDAO {
    RealmResults<Dog> addDogs(String string);

    RealmResults<Dog> readListDogs();

    void deleteDB();
}
