package com.aleksandr.dbpetsrealm.model;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class DogRealm implements DogDAO {

    private Realm realm;

    private static DogRealm instance = null;

    public static DogRealm getInstance(Context appContext) {
        if (instance == null) {
            instance = new DogRealm();
        }
        return instance;
    }

    public RealmResults<Dog> addDogs(String name) {
        this.realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        final RealmQuery<Dog> query = realm.where(Dog.class);
        Dog dog = realm.createObject(Dog.class);
        dog.setName(name);
        RealmResults<Dog> dogs = query.findAll();
        realm.commitTransaction();
        realm.close();
        return dogs;
    }

    public RealmResults<Dog> readListDogs() {
        this.realm = Realm.getDefaultInstance();
        final RealmResults<Dog> results = realm.where(Dog.class).findAll();
        realm.close();
        return results;
    }

    public void deleteDB() {
        this.realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        final RealmResults<Dog> results = realm.where(Dog.class).findAll();
        results.deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }
}
