package com.aleksandr.dbpetsrealm;

import android.app.Application;

import com.aleksandr.dbpetsrealm.model.Dog;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MyApplication extends Application {

    private Realm realm;

//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Realm.init(this);
//        RealmConfiguration configuration =
//                new RealmConfiguration.Builder().name("myrealm.realm").build();
//        Realm.setDefaultConfiguration(configuration);
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        realm = Realm.getDefaultInstance();
        final RealmQuery<Dog> query = realm.where(Dog.class);
        RealmResults<Dog> dogs = query.findAll();


    }
}
