package com.aleksandr.dbpetsrealm;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleksandr.dbpetsrealm.model.Dog;

import java.util.List;

public class DogsAdapter extends RecyclerView.Adapter<DogsAdapter.ViewHolder> {

    private List<Dog> dogs;

    public DogsAdapter(List<Dog> dogs) {
        this.dogs = dogs;
        notifyDataSetChanged();
    }

    public void dataUpdate(List<Dog> dogs) {
        if (dogs != null) {
            this.dogs = dogs;
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_dogs, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(dogs.get(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return dogs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDogsName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDogsName = itemView.findViewById(R.id.tv_dogs_adapter);
        }

        private void bindTo(final Dog dog) {
            tvDogsName.setText(dog.getName());
        }
    }
}
