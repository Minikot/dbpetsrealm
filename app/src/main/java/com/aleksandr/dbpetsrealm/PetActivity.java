package com.aleksandr.dbpetsrealm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aleksandr.dbpetsrealm.model.Dog;
import com.aleksandr.dbpetsrealm.model.DogFactory;

import java.util.ArrayList;
import java.util.List;

public class PetActivity extends AppCompatActivity {

    private EditText etName;
    private RecyclerView rvDogs;
    private DogsAdapter dogsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);

        etName = findViewById(R.id.et_setName);

        Button btnAddDog = findViewById(R.id.btn_add_pet);
        Button btnDelList = findViewById(R.id.btn_del_list);

        rvDogs = findViewById(R.id.rv_dogsRV);
        dogsAdapter = new DogsAdapter(new ArrayList<Dog>());
        rvDogs.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        rvDogs.setAdapter(dogsAdapter);

        dogsAdapter.dataUpdate(DogFactory.getDogDAO(getApplicationContext()).readListDogs());

        /** Button AddDog */
        btnAddDog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameDog = etName.getText().toString();
                if (nameDog.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter the name!!!", Toast.LENGTH_LONG).show();

                } else {
                    List<Dog> dogs = DogFactory.getDogDAO(getApplicationContext()).addDogs(nameDog);
                    dogsAdapter.dataUpdate(dogs);
                }
            }
        });

        /** Button DelList */
        btnDelList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DogFactory.getDogDAO(getApplicationContext()).deleteDB();
                List<Dog> dogs = DogFactory.getDogDAO(getApplicationContext()).readListDogs();
                dogsAdapter.dataUpdate(dogs);
            }
        });
    }
}
